# DOM
Projet dans lequel on voit le DOM avec typescript

## Exercices

### DOM Selector ([html](public/index.html)/[ts](/src/index.ts))
1. Mettre le texte de l'élément avec l'id para2 en bleu
2. Mettre une border en pointillet noire à la section2
3. Mettre une background color orange à l'élément de la classe colorful de la section2 
4. Mettre le h2 de la section1 en italique
5. Cacher l'élément colorful situé dans un paragraphe
6. Changer le texte de para2 pour "modified by JS"
7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
8. Rajouter la classe big-text sur le h2 de la section2
9. Bonus : Faire que tous les paragraphe du document soit en italique

### Exo Counter ([html](public/exo-counter.html)/[ts](../../src/exo-counter.ts))
1. Créer les fichier exo-counter.html / .ts
2. Dans le le html, rajouter 2 button, un + et un -
3. Dans le ts, créer une variable counter initialisée à zéro
4. Capturer les 2 button et avec des addEventListener faire que quand on click sur l'un ça incrémente le counter et ça affiche sa valeur en console
5. Faire que quand on click sur l'autre, ça fasse pareil, mais en décrémentant
6. Rajouter un élément span, ou div ou osef dans le html et lui mettre 0 dedans
7. Modifier nos eventListener pour faire qu'à la place ou en plus du console log, ça  aille modifier le innerHTML/textContent de l'élément en question pour lui mettre la valeur actuelle du counter 

Bonus : Créer un bouton reset. Rajouter une limite au compteur pour qu'il puisse pas aller en dessous de zéro par exemple (avec pourquoi pas le bouton - qui devient non clickable quand c'est le cas)

### Amazing moving square ([html](public/exo-square.html)/[ts](/src/exo-square.ts))
1. Créer un nouveau fichier html/ts pour exo-square
2. Dans le html, créer une div#playground et lui mettre en css  une height à 100vh, et une position relative. Dans cette div, rajouter une autre div#square et faire en sorte via le css qu'elle ressemble à un carré rouge (ou autre chose, on s'en fout un peu) et qu'elle soit en position absolute
3. Dans le TS, capturer le playground et le square puis rajouter un addEventListener au click sur le playground
4. Dans ce listener, rajouter un argument event à la fat-arrow, regarder via le console log si le event ne contiendrait pas des valeurs indiquant où on a cliqué (spoiler : c'est le cas)
5. Utiliser ces valeurs pour les assigner en top et en left de notre square (il va falloir concaténé 'px' aux valeurs en question)
6. On peut rajouter une petite transition à notre css sur le carré pour qu'il glisse tel un cygne sur un lac gelé 

Bonus: Faire un autre carré qui suit la souris dès qu'elle bouge.

Bonus du bonus : faire qu'il suive la souris seulement quand on a le click appuyé dessus (un drag'n drop quoi)


### Calculus DOM ([html](public/exo-calculus.html)/[ts](src/exo-calculus.ts))
1. Dans le fichier HTML, rajouter un form qui contiendra 2 input type number ainsi qu'un span qui contiendra le résultat du calcul, ainsi qu'un button = qui lancera le calcul
2. Côté typescript, faire en sorte de rajouter un eventListener sur le submit du formulaire qui va récupérer la propriété value de chaque input, et les afficher en console
3. Pour que le formulaire ne recharge pas la page, il va falloir rajouter l'argument event et dans la fonction commencer par déclencher un event.preventDefault()
4. Une fois qu'on a les valeurs dans la console, faire en sorte de les additionner et afficher le résultat en console
5. Quand ça marche, faire en sorte d'assigner ce résultat au span
6. Rajouter dans le html un select qui aura comme options + - / et *, modifier l'eventListener pour faire qu'on récupère la value de ce select et selon l'opérateur on fait un calcul ou un autre
7. Externaliser la logique du calcul dans une fonction qui attendra 3 arguments, les deux nombres et l'opérateur 



### Texte pseudo Prédictif ([html](public/exo-predict.html)/[ts](src/exo-predict.ts))
1. Dans un fichier exo-predict.html créer un paragraphe et 5 button tous avec une class "prediction" et qui contiendront "hello" "bye" "yes" "no" "please"
2. Côté typescript, sélectionner tous ces button avec un querySelectorAll
3. Faire une boucle sur ces buttons, et dans la boucle, faire un addEventListener sur chaque button qui fera pour le moment juste un console.log de n'importe quoi
4. Modifier le console.log pour faire que plutôt que n'importe quoi, il affiche le text du bouton qui a été cliqué
5. Concaténer le texte en question dans le paragraphe
6. Rajouter un input type text dans le html et le récupérer dans le typescript
7. Faire un eventListener sur l'event "input" (qui se déclenche à chaque fois que la valeur de l'input change) qui va prendre la valeur actuelle de l'input puis faire une boucle sur les buttons et faire en sorte de n'afficher que les bouton dont le text contient la valeur de l'input (il y a une fonction text.includes('blup') qui renvoie true ou false si la variable text contient 'blup') 
