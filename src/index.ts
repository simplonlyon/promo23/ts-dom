/*
le document.querySelector permet de chercher dans le document HTML actuel un élément
en se servant d'un sélecteur CSS (si plusieurs éléments matchent ce sélecteur, seul le
premier sera renvoyé).
Par défaut, cette fonction renvoie un Element qui est un type assez vague, on peut 
préciser le type de balise qu'on s'attend à récupérer en rajoutant les chevrons
et le type attendu : document.querySelector<HTMLParagraphElement>('...'). Si le type
est plus précis, l'autocomplétion sera plus précise aussi. En règle général HTMLElement
fait l'affaire, juste pour certaines propriétés il faudra être plus précis (genre les 
href ou les src)
*/

let para2 = document.querySelector<HTMLElement>('#para2');
let section2 = document.querySelector<HTMLElement>('#section2');
let colorfulSect2 = document.querySelector<HTMLElement>('#section2 .colorful');
let h2Sect1 = document.querySelector<HTMLElement>('#section1 h2');
let colorfulInP = document.querySelector<HTMLElement>('p .colorful');
let aSect1 = document.querySelector<HTMLAnchorElement>('#section1 a');
let h2Sect2 = document.querySelector<HTMLElement>('#section2 h2');
let paras = document.querySelectorAll<HTMLElement>('p');

//Comme le querySelector renvoie potentiellement rien si l'élément n'existe pas, on
//commence par tester avec un if s'il a bien été trouvé
if(para2) {
    para2.style.color = 'blue';
    para2.textContent = 'modified by JS';
}


if(section2) {
    section2.style.border = '1px dashed black';
}


if(colorfulSect2) {
    colorfulSect2.style.backgroundColor = 'orange';
}


if(h2Sect1) {
    h2Sect1.style.fontStyle = 'italic';
}


if(colorfulInP) {
    colorfulInP.style.display = 'none';
}

if(aSect1) {
    aSect1.href = 'https://lyon.simplon-ara.fr';
}

if(h2Sect2) {
   //Ajouter une classe sur un élément HTML, possible d'utiliser className aussi, mais classList a plus de méthodes sympas
    h2Sect2.classList.add('big-text');
}
//Vu que paras est le résultat d'un querySelectorAll, il contient un array d'élément
//Il faut donc itérer dessus avec un for pour appliquer un style à chaque élément
for (const itemPara of paras) {
    itemPara.style.fontStyle = 'italic';
}

/**
 * Fonction qui va faire un query sélector et déclencher une
 * erreur (et donc stopper l'exécution du code) si aucun
 * élément n'est trouvé
 * @param selector le sélecteur CSS de l'élément à récupérer
 * @returns Le premier élément HTML matchant le sélecteur
 */
function queryOrError(selector:string):HTMLElement {
    let element = document.querySelector<HTMLElement>(selector);
    if(!element) {
        throw new Error('Element for selector : '+selector+' not found');
    }
    return element;
}