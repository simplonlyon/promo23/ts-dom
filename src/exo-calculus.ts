const form = document.querySelector<HTMLFormElement>('form');
const inputA = document.querySelector<HTMLInputElement>('#a');
const inputB = document.querySelector<HTMLInputElement>('#b');
const spanResult = document.querySelector<HTMLElement>('#result');
const selectOperator = document.querySelector<HTMLSelectElement>('#operator');

form?.addEventListener('submit', (event) => {
    event.preventDefault();
    let operator = selectOperator?.value;
    let result = 0;
    let a = Number(inputA?.value);
    let b = Number(inputB?.value);
    result = calculus(operator, a, b);
    
    
    if(spanResult) {
        spanResult.innerHTML = String(result);
    }
})
/**
 * Une fonction qui fait un calcul
 * @param operator l'opérateur parmis + - / ou *
 * @param a le premier nombre
 * @param b le deuxième nombre
 * @returns le résultat du calcul ou NaN si pas un opérateur valide
 */
function calculus(operator: string | undefined, a: number, b: number):number {
    if (operator == '+') {
        return a + b;
    }
    if (operator == '-') {
        return a - b;
    }
    if (operator == '*') {
        return a * b;
    }
    if (operator == '/') {
        return a / b;
    }
    return NaN;
}
