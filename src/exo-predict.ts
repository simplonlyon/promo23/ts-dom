const buttons = document.querySelectorAll<HTMLButtonElement>('.prediction');
const para = document.querySelector('p');
const input = document.querySelector('input');

for (const itemButton of buttons) {
    itemButton.addEventListener('click', () => {
        if(para) {
            para.innerHTML += ' '+itemButton.textContent;
        }
    });
}

input?.addEventListener('input', () => {
    console.log(input.value)
    for (const itemButton of buttons) {
        if(!itemButton.textContent?.includes(input.value)) {
            itemButton.style.display = 'none';
        } else {
            itemButton.style.display = 'inline';
        }
    }
})