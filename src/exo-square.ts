const square = document.querySelector<HTMLElement>('#square');
const square2 = document.querySelector<HTMLElement>('#square2');
const playground = document.querySelector<HTMLElement>('#playground');

playground?.addEventListener('click', (event) => {
    console.log(event);

    
    if(square) {
        square.style.left = event.clientX+'px';
        square.style.top = event.clientY+'px';
    }
});


playground?.addEventListener('mousemove', (event) => {
    if(square2 && event.buttons > 0 && event.target == square2) {
        square2.style.left = event.clientX-50+'px';
        square2.style.top = event.clientY-50+'px';
    }
});