/*
On commence par capturer nos différents élément HTML et les mettre dans des constantes
(pasqu'à priori on aura pas besoin des les modifier)
*/
const btnMinus = document.querySelector<HTMLButtonElement>('#minus');
const btnPlus = document.querySelector<HTMLButtonElement>('#plus');
const btnReset = document.querySelector<HTMLButtonElement>('#reset');
const span = document.querySelector<HTMLSpanElement>('span');
//On initialise une variable qui contiendra la valeur du compteur
let counter = 0;


//On rajoute un event au click sur le bouton plus dans lequel on va incrémenter le counter
//puis mettre à jour le text du span pour lui assigner la nouvelle valeur du counter (convertie en string)
btnPlus?.addEventListener('click', () => {
    counter++;
    console.log(counter);

    updateHTML();
});


btnMinus?.addEventListener('click', () => {

    counter--;
    console.log(counter);
    updateHTML();


});

btnReset?.addEventListener('click', () => {
    counter = 0;
    console.log(counter);

    updateHTML();
});
/**
 * Fonction qui va mettre à jour le html en fonction de la valeur du counter
 * (désactive le bouton moins si le counter vaut zéro et met à jour l'affichage de
 * la valeur du span)
 */
function updateHTML() {
    if (span) {
        span.textContent = String(counter);
    }

    if (btnMinus) {
        btnMinus.disabled = counter == 0;
    }
}

